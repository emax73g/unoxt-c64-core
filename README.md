# UnoXT Commodore 64 Core

This is port of Neuro 999 ZXDos C64 Core 

https://github.com/neurorulez/jammazx1/tree/master/ordenadores/Comodore64

-------

Put *.d64 Disk Images to the SD Card

-------

Scroll Lock - toggle 15 KHz, 30 KHz HSYNC

F12 - OSD

F10 - Reset

-------

F12/Load Disk/*.d64

LOAD"$",8

LIST

LOAD"filename*",8 or

LOAD"*",8

RUN

-------
